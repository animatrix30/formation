// Event
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class UserBlocEvent {}

class ReinitUserFormEvent extends UserBlocEvent {}

class CreateUserEvent extends UserBlocEvent {
  final String firstName;
  final String lastName;
  final String email;
  final String phoneNumber;

  CreateUserEvent({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.phoneNumber,
  });
}

// Bloc
class UserBloc extends Bloc<UserBlocEvent, UserState> {
  UserBloc() : super(const InitialUserState()) {
    on<ReinitUserFormEvent>(_onReinitForm);
    on<CreateUserEvent>(_onCreateUser);
  }

  Future<void> _onReinitForm(
    ReinitUserFormEvent event,
    Emitter<UserState> emit,
  ) async {
    emit(const InitialUserState());
  }

  Future<void> _onCreateUser(
    CreateUserEvent event,
    Emitter<UserState> emit,
  ) async {
    ErrorUserType? firstNameError = _checkFirstName(event.firstName);
    ErrorUserType? lastNameError = _checkLastName(event.lastName);
    ErrorUserType? emailError = _checkEmail(event.email);
    ErrorUserType? phoneNumberError = _checkPhoneNumber(event.phoneNumber);

    if ([
          firstNameError,
          lastNameError,
          emailError,
          phoneNumberError,
        ].where((element) => element == null).length ==
        4) {
      try {
        UserCredential credential =
            await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: event.email,
          password: '123456',
        );

        // Mot de passe perdu
        // await FirebaseAuth.instance.sendPasswordResetEmail(email: event.email);

        // Se connecter
        // await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);

        emit(const CreatedUserState());
      } catch (e) {
        emit(ErrorUserState(error: e));
      }
    } else {
      emit(ErrorUserState(
        firstName: firstNameError,
        lastName: lastNameError,
        email: emailError,
        phoneNumber: phoneNumberError,
      ));
    }
  }

  ErrorUserType? _checkFirstName(String firstName) {
    if (firstName.isEmpty) {
      return ErrorUserType.empty;
    } else if (firstName.length < 3) {
      return ErrorUserType.incorrect;
    }
    return null;
  }

  ErrorUserType? _checkLastName(String lastName) {
    if (lastName.isEmpty) {
      return ErrorUserType.empty;
    } else if (lastName.length < 3) {
      return ErrorUserType.incorrect;
    }
    return null;
  }

  ErrorUserType? _checkEmail(String email) {
    if (email.isEmpty) {
      return ErrorUserType.empty;
    } else if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(email)) {
      return ErrorUserType.incorrect;
    }
    return null;
  }

  ErrorUserType? _checkPhoneNumber(String phoneNumber) {
    if (phoneNumber.isEmpty) {
      return ErrorUserType.empty;
    } else if (!(phoneNumber.startsWith('+33') ||
        phoneNumber.startsWith('0'))) {
      return ErrorUserType.incorrect;
    }
    return null;
  }
}

// State
sealed class UserState {
  const UserState();
}

class InitialUserState extends UserState {
  const InitialUserState();
}

class CreatedUserState extends UserState {
  const CreatedUserState();
}

class ErrorUserState extends UserState {
  final ErrorUserType? firstName;
  final ErrorUserType? lastName;
  final ErrorUserType? email;
  final ErrorUserType? phoneNumber;
  final dynamic error;

  ErrorUserState({
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
    this.error,
  });
}

enum ErrorUserType { empty, incorrect }
