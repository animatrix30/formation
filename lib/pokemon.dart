import 'package:json_annotation/json_annotation.dart';

part 'pokemon.g.dart';

@JsonSerializable()
class PokemonResponse {
  @JsonKey(name: 'base_experience')
  final int baseExperience;

  @JsonKey(name: 'is_default')
  final bool isDefault;

  @JsonKey(name: 'cries')
  final PokemonResponseCries cries;

  @JsonKey(name: 'moves')
  final List<PokemonResponseMove> moves;

  PokemonResponse(
    this.baseExperience,
    this.isDefault,
    this.cries,
    this.moves,
  );

  factory PokemonResponse.fromJson(Map<String, dynamic> json) =>
      _$PokemonResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonResponseToJson(this);

  @override
  String toString() {
    return 'PokemonResponse{baseExperience: $baseExperience, isDefault: $isDefault, cries: $cries, moves: $moves}';
  }
}

@JsonSerializable()
class PokemonResponseCries {
  @JsonKey(name: 'latest')
  final String latest;

  PokemonResponseCries(this.latest);

  factory PokemonResponseCries.fromJson(Map<String, dynamic> json) =>
      _$PokemonResponseCriesFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonResponseCriesToJson(this);

  @override
  String toString() {
    return 'PokemonResponseCries{latest: $latest}';
  }
}

@JsonSerializable()
class PokemonResponseMove {
  @JsonKey(name: 'move')
  final PokemonResponseMoveInternal move;

  PokemonResponseMove(this.move);

  factory PokemonResponseMove.fromJson(Map<String, dynamic> json) =>
      _$PokemonResponseMoveFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonResponseMoveToJson(this);

  @override
  String toString() {
    return 'PokemonResponseMove{move: $move}';
  }
}

@JsonSerializable()
class PokemonResponseMoveInternal {
  @JsonKey(name: 'url')
  final String? url;

  PokemonResponseMoveInternal(this.url);

  factory PokemonResponseMoveInternal.fromJson(Map<String, dynamic> json) =>
      _$PokemonResponseMoveInternalFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonResponseMoveInternalToJson(this);

  @override
  String toString() {
    return 'PokemonResponseMoveInternal{url: $url}';
  }
}
