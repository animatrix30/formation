import 'package:json_annotation/json_annotation.dart';

part 'product_response.g.dart';

@JsonSerializable()
class ProductAPIModel {
  @JsonKey(name: 'response')
  final ProductAPIModelResponse response;

  ProductAPIModel(this.response);

  factory ProductAPIModel.fromJson(Map<String, dynamic> json) =>
      _$ProductAPIModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductAPIModelToJson(this);

  @override
  String toString() {
    return 'ProductAPIModel{response: $response}';
  }
}

@JsonSerializable()
class ProductAPIModelResponse {
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'brands')
  final List<String> brands;

  ProductAPIModelResponse(this.name, this.brands);

  factory ProductAPIModelResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductAPIModelResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductAPIModelResponseToJson(this);

  @override
  String toString() {
    return 'ProductAPIModelResponse{name: $name, brands: $brands}';
  }
}
