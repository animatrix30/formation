import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:untitled12/users_bloc.dart';

class CreateUser extends StatefulWidget {
  const CreateUser({super.key});

  @override
  State<CreateUser> createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser> {
  final GlobalKey<FormState> _form = GlobalKey();
  final TextEditingController _firstName = TextEditingController();

  final TextEditingController _lastName = TextEditingController();

  final TextEditingController _email = TextEditingController();

  final TextEditingController _phoneNumber = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocProvider(
          create: (_) => UserBloc(),
          child: BlocListener<UserBloc, UserState>(
            listener: (BuildContext context, UserState state) async {
              if (state is InitialUserState) {
                _firstName.text = '';
                _lastName.text = '';
                _email.text = '';
                _phoneNumber.text = '';
              } else if (state is ErrorUserState && state.error != null) {
                var res = await showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Erreur : ${state.error}'),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop(true);
                              },
                              child: const Text('Oui')),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop(false);
                              },
                              child: const Text('non'))
                        ],
                      );
                    });

                if (res == true) {
                  print('OK');
                }
              } else if (state is CreatedUserState) {
                GoRouter.of(context).pushReplacement('/test');
              }
            },
            child: Form(
              key: _form,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text("Prénom"),
                  TextFormField(
                    controller: _firstName,
                  ),
                  BlocBuilder<UserBloc, UserState>(
                      builder: (BuildContext context, UserState state) {
                    if (state is ErrorUserState && state.firstName != null) {
                      return Text(
                        switch (state.firstName!) {
                          ErrorUserType.empty =>
                            'Le prénom ne peut pas être vide',
                          ErrorUserType.incorrect =>
                            'Le prénom est incorrect (inférieur à 3 caractères)',
                        },
                        style: const TextStyle(color: Colors.red),
                      );
                    } else {
                      return const SizedBox.shrink();
                    }
                  }),
                  const Text("Nom"),
                  TextFormField(
                    controller: _lastName,
                  ),
                  BlocBuilder<UserBloc, UserState>(
                      builder: (BuildContext context, UserState state) {
                    if (state is ErrorUserState && state.lastName != null) {
                      return Text(
                        switch (state.lastName!) {
                          ErrorUserType.empty => 'Le nom ne peut pas être vide',
                          ErrorUserType.incorrect =>
                            'Le nom est incorrect (inférieur à 3 caractères)',
                        },
                        style: const TextStyle(color: Colors.red),
                      );
                    } else {
                      return const SizedBox.shrink();
                    }
                  }),
                  const Text("Email"),
                  TextFormField(
                    controller: _email,
                    keyboardType: TextInputType.emailAddress,
                  ),
                  BlocBuilder<UserBloc, UserState>(
                      builder: (BuildContext context, UserState state) {
                    if (state is ErrorUserState && state.email != null) {
                      return Text(
                        switch (state.email!) {
                          ErrorUserType.empty =>
                            'L\'email ne peut pas être vide',
                          ErrorUserType.incorrect =>
                            'Le format de l\'email est incorrect',
                        },
                        style: const TextStyle(color: Colors.red),
                      );
                    } else {
                      return const SizedBox.shrink();
                    }
                  }),
                  const Text("Téléphone"),
                  TextFormField(
                    controller: _phoneNumber,
                    keyboardType: TextInputType.phone,
                    validator: (String? value) {
                      // Pour info
                      if (value?.startsWith('+33') == true) {
                        return null;
                      }
                      return 'KO';
                    },
                  ),
                  BlocBuilder<UserBloc, UserState>(
                      builder: (BuildContext context, UserState state) {
                    if (state is ErrorUserState && state.phoneNumber != null) {
                      return Text(
                        switch (state.phoneNumber!) {
                          ErrorUserType.empty =>
                            'Le numéro de téléphone ne peut pas être vide',
                          ErrorUserType.incorrect =>
                            'Le numéro de téléphone est incorrect',
                        },
                        style: const TextStyle(color: Colors.red),
                      );
                    } else {
                      return const SizedBox.shrink();
                    }
                  }),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Builder(builder: (context) {
                        return Row(
                          children: [
                            TextButton(
                              onPressed: () {
                                BlocProvider.of<UserBloc>(context).add(
                                  ReinitUserFormEvent(),
                                );
                              },
                              style: TextButton.styleFrom(
                                backgroundColor: Colors.red,
                                foregroundColor: Colors.white,
                              ),
                              child: const Text('Réinitialiser'),
                            ),
                            const SizedBox(width: 15.0),
                            TextButton(
                              onPressed: () {
                                BlocProvider.of<UserBloc>(context).add(
                                  CreateUserEvent(
                                    firstName: _firstName.text,
                                    lastName: _lastName.text,
                                    email: _email.text,
                                    phoneNumber: _phoneNumber.text,
                                  ),
                                );
                              },
                              style: TextButton.styleFrom(
                                backgroundColor: Colors.red,
                                foregroundColor: Colors.white,
                              ),
                              child: const Text('Envoyer'),
                            ),
                          ],
                        );
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
