// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductAPIModel _$ProductAPIModelFromJson(Map<String, dynamic> json) =>
    ProductAPIModel(
      ProductAPIModelResponse.fromJson(
          json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProductAPIModelToJson(ProductAPIModel instance) =>
    <String, dynamic>{
      'response': instance.response,
    };

ProductAPIModelResponse _$ProductAPIModelResponseFromJson(
        Map<String, dynamic> json) =>
    ProductAPIModelResponse(
      json['name'] as String,
      (json['brands'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ProductAPIModelResponseToJson(
        ProductAPIModelResponse instance) =>
    <String, dynamic>{
      'name': instance.name,
      'brands': instance.brands,
    };
