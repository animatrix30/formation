// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PokemonResponse _$PokemonResponseFromJson(Map<String, dynamic> json) =>
    PokemonResponse(
      json['base_experience'] as int,
      json['is_default'] as bool,
      PokemonResponseCries.fromJson(json['cries'] as Map<String, dynamic>),
      (json['moves'] as List<dynamic>)
          .map((e) => PokemonResponseMove.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PokemonResponseToJson(PokemonResponse instance) =>
    <String, dynamic>{
      'base_experience': instance.baseExperience,
      'is_default': instance.isDefault,
      'cries': instance.cries,
      'moves': instance.moves,
    };

PokemonResponseCries _$PokemonResponseCriesFromJson(
        Map<String, dynamic> json) =>
    PokemonResponseCries(
      json['latest'] as String,
    );

Map<String, dynamic> _$PokemonResponseCriesToJson(
        PokemonResponseCries instance) =>
    <String, dynamic>{
      'latest': instance.latest,
    };

PokemonResponseMove _$PokemonResponseMoveFromJson(Map<String, dynamic> json) =>
    PokemonResponseMove(
      PokemonResponseMoveInternal.fromJson(
          json['move'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PokemonResponseMoveToJson(
        PokemonResponseMove instance) =>
    <String, dynamic>{
      'move': instance.move,
    };

PokemonResponseMoveInternal _$PokemonResponseMoveInternalFromJson(
        Map<String, dynamic> json) =>
    PokemonResponseMoveInternal(
      json['url'] as String?,
    );

Map<String, dynamic> _$PokemonResponseMoveInternalToJson(
        PokemonResponseMoveInternal instance) =>
    <String, dynamic>{
      'url': instance.url,
    };
