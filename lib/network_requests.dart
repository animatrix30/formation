import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:untitled12/product_response.dart';

part 'network_requests.g.dart';

@RestApi()
abstract class _OFFAPI {
  factory _OFFAPI(Dio dio, {required String baseUrl}) = __OFFAPI;

  @GET('/getProduct')
  Future<ProductAPIModel> loadProduct({
    @Query('barcode') required String barcode,
    CancelToken? token,
  });
}

class OFFAPI {
  static final OFFAPI _singleton = OFFAPI._internal();

  factory OFFAPI() {
    return _singleton;
  }

  OFFAPI._internal();

  final _OFFAPI _api = _OFFAPI(
    Dio(),
    baseUrl: 'https://api.formation-android.fr/v2/',
  );

  Future<ProductAPIModel> loadProduct(String barcode) async {
    assert(barcode.isNotEmpty);
    return _api.loadProduct(barcode: barcode);
  }
}
