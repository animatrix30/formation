import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:untitled12/counter_bloc.dart';
import 'package:untitled12/firebase_options.dart';
import 'package:untitled12/network_requests.dart';
import 'package:untitled12/product_response.dart';
import 'package:untitled12/res/app_colors.dart';
import 'package:untitled12/res/app_icons.dart';
import 'package:untitled12/res/app_images.dart';
import 'package:untitled12/res/app_vectorial_images.dart';
import 'package:untitled12/users.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  GoRouter _router = GoRouter(routes: [
    GoRoute(
      path: '/',
      builder: (
        BuildContext context,
        GoRouterState state,
      ) {
        return CreateUser();
      },
      routes: [
        GoRoute(
          path: 'test',
          builder: (
            BuildContext context,
            GoRouterState state,
          ) {
            return const StackDemo();
          },
        ),
      ],
    ),
  ]);

  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return TestColor(
      color: Colors.orange,
      child: MaterialApp.router(
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: 'Avenir',
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
          useMaterial3: true,
          textTheme: const TextTheme(
            headlineMedium: TextStyle(
                fontSize: 17.0,
                color: AppColors.blueDark,
                fontWeight: FontWeight.bold),
          ),
        ),
        routerConfig: _router,
      ),
    );
  }
}

class AddYourCard extends StatelessWidget {
  const AddYourCard({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.sizeOf(context);

    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(AppImages.nutriscoreA),
            SizedBox(height: size.height * 0.1),
            const Text('Let\'s add your card', textAlign: TextAlign.center),
            SizedBox(height: size.height * 0.01),
            const Text(
              'Experience the power of financial organization with our platform.',
              textAlign: TextAlign.center,
            ),
            SizedBox(height: size.height * 0.2),
            TextButton(
              onPressed: () {},
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  const Color(0xFF354ff4),
                ),
                foregroundColor: MaterialStateProperty.all(
                  Colors.white,
                ),
              ),
              child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.add),
                  SizedBox(width: 8.0),
                  Text('Add your card'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyCart extends StatelessWidget {
  const MyCart({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.sizeOf(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mes scans'),
        centerTitle: false,
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(AppIcons.barcode),
          )
        ],
      ),
      body: SizedBox.expand(
        child: Column(
          children: [
            const Spacer(
              flex: 10,
            ),
            SvgPicture.asset(
              AppVectorialImages.appLogo,
            ),
            const Spacer(
              flex: 50,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: size.height * 0.25),
              child: const Text(
                'Vous n\'avez pas encore scanné de produit',
                textAlign: TextAlign.center,
              ),
            ),
            const Spacer(
              flex: 30,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: size.height * 0.25),
              child: OutlinedButton(
                onPressed: () {},
                style: OutlinedButton.styleFrom(
                  foregroundColor: AppColors.blue,
                  backgroundColor: AppColors.yellow,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(22.0))),
                ),
                child: SizedBox(
                  width: size.width * 0.2,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(child: Text('Commencer'.toUpperCase())),
                      const Expanded(child: Icon(Icons.arrow_right_alt)),
                    ],
                  ),
                ),
              ),
            ),
            const Spacer(
              flex: 10,
            ),
          ],
        ),
      ),
    );
  }
}

class StackDemo extends StatelessWidget {
  const StackDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IconTheme(
        data: const IconThemeData(color: Colors.white),
        child: SizedBox.expand(
          child: Stack(
            children: [
              Positioned(
                top: 0.0,
                left: 0.0,
                right: 0.0,
                height: 300.0,
                child: Image.network(
                  'https://plus.unsplash.com/premium_photo-1663858367001-89e5c92d1e0e?q=80&w=2515&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                  fit: BoxFit.cover,
                ),
              ),
              const ProductContent(),
              const Positioned(
                top: 10.0,
                left: 10.0,
                child: BackButton(),
              ),
              Positioned(
                top: 10.0,
                right: 10.0,
                child: IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.share),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProductContent extends StatelessWidget {
  const ProductContent({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        constraints: BoxConstraints(
          minHeight: MediaQuery.sizeOf(context).height,
        ),
        margin: const EdgeInsets.only(top: 280.0),
        padding: const EdgeInsets.only(
          top: 30.0,
          left: 20.0,
          right: 20.0,
        ),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(16.0),
          ),
        ),
        child: const Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Petits pois et carottes'),
            Text(
              'Carottes',
              style: TextStyle(color: AppColors.gray2),
            ),
            ProductScores(),
          ],
        ),
      ),
    );
  }
}

class ProductScores extends StatelessWidget {
  const ProductScores({super.key});

  @override
  Widget build(BuildContext context) {
    return const ColoredBox(
      color: AppColors.gray1,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 40,
                  child: NutriScore(),
                ),
                VerticalDivider(),
                Expanded(
                  flex: 60,
                  child: NovaScore(),
                ),
              ],
            ),
          ),
          Divider(),
          EcoScore(),
        ],
      ),
    );
  }
}

class NutriScore extends StatelessWidget {
  const NutriScore({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: ScoreTitle(text: 'Nutri-Score'),
          ),
          Image.asset(
            AppImages.nutriscoreA,
            height: 100,
          ),
        ],
      ),
    );
  }
}

class NovaScore extends StatelessWidget {
  const NovaScore({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScoreTitle(
            text: 'Groupe NOVA',
          ),
          Text('Produits alimentaires et boissons ultra-transformés'),
        ],
      ),
    );
  }
}

class EcoScore extends StatelessWidget {
  const EcoScore({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScoreTitle(text: 'Eco-Score'),
          Row(children: [
            Icon(
              AppIcons.ecoscore_d,
              color: AppColors.ecoScoreD,
            ),
            SizedBox(width: 8.0),
            Expanded(child: Text('Impact environnemental élevé'))
          ])
        ],
      ),
    );
  }
}

class ScoreTitle extends StatelessWidget {
  final String text;

  const ScoreTitle({
    required this.text,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 1.0),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headlineMedium,
      ),
    );
  }
}

class RequestExample extends StatefulWidget {
  const RequestExample({super.key});

  @override
  State<RequestExample> createState() => _RequestExampleState();
}

class _RequestExampleState extends State<RequestExample> {
  ProductAPIModel? response;

  @override
  void initState() {
    super.initState();
    sendRequest();
  }

  Future<void> sendRequest() async {
    setState(() {
      response = null;
    });

    Future.delayed(const Duration(seconds: 5));

    response = await OFFAPI().loadProduct("5000159484695");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    print(TestColor.of(context).color);

    return Scaffold(
      body: Center(
        child: _getContent(),
      ),
    );
  }

  Widget _getContent() {
    if (response == null) {
      return const CircularProgressIndicator();
    } else {
      return InkWell(
          onTap: () {
            sendRequest();
          },
          child: Text(response!.toString()));
    }
  }
}

class TestColor extends InheritedWidget {
  const TestColor({
    super.key,
    required Widget child,
    required this.color,
  }) : super(child: child);

  final Color color;

  static TestColor of(BuildContext context) {
    final TestColor? result =
        context.dependOnInheritedWidgetOfExactType<TestColor>();
    assert(result != null, 'No TestColor found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(TestColor old) {
    return old.color != color;
  }
}

class CounterExample extends StatelessWidget {
  const CounterExample({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CounterBloc(),
      child: Builder(builder: (context) {
        return Scaffold(
          body: Center(
            child: BlocBuilder<CounterBloc, CounterState>(
              builder: (BuildContext context, CounterState state) {
                return Text(state.counter.toString());
              },
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              BlocProvider.of<CounterBloc>(context)
                  .add(DecrementCounterEvent());
            },
            child: const Icon(Icons.add),
          ),
        );
      }),
    );
  }
}
