// Event
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class CounterEvent {}

class IncrementCounterEvent extends CounterEvent {}

class DecrementCounterEvent extends CounterEvent {}

// BLoC
class CounterBloc extends Bloc<CounterEvent, CounterState> {
  // Etat initial
  CounterBloc() : super(CounterState.init()) {
    on<IncrementCounterEvent>(_onIncrement);
    on<DecrementCounterEvent>(_onDecrement);
  }

  Future<void> _onIncrement(
    CounterEvent event,
    Emitter<CounterState> emit,
  ) async {
    final int counter = state.counter;
    emit(CounterState(counter + 1));
  }

  Future<void> _onDecrement(
    CounterEvent event,
    Emitter<CounterState> emit,
  ) async {
    final int counter = state.counter;
    emit(CounterState(counter - 1));
  }
}

// State
class CounterState {
  final int counter;

  CounterState(this.counter);

  CounterState.init() : counter = 0;
}
